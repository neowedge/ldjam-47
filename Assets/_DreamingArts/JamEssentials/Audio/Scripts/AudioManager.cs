﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField]
        private AudioClip _titleMusicClip;

        [SerializeField]
        private AudioClip _gameMusicClip;

        [SerializeField]
        private AudioClip _selectClip;

        [SerializeField]
        private AudioClip _warningClip;

        [SerializeField]
        private AudioClip _popupOpenClip;

        [SerializeField]
        private AudioClip _popupCloseClip;

        [SerializeField]
        private AudioClip _showTextClip;

        [SerializeField]
        private AudioClip _startGameClip;

        [SerializeField]
        private AudioClip _shotClip;

        [SerializeField]
        private AudioClip _takeDamageClip;

        [SerializeField]
        private AudioClip _enemyDestroyClip;

        [SerializeField]
        private AudioClip _enemyTakeDamageClip;

        [SerializeField]
        private AudioClip _playerDestroyClip;

        //[System.NonSerialized]
        //protected GameObject _container;

        private AudioSource _musicSource;
        private List<AudioSource> _audioSources;

        protected AudioSource GetAudioSource()
        {
            for (int i = 0; i < _audioSources.Count; ++i)
            {
                if (!_audioSources[i].isPlaying)
                {
                    return _audioSources[i];
                }
            }

            AudioSource newSource = gameObject.AddComponent<AudioSource>();
            newSource.loop = false;
            newSource.playOnAwake = false;
            _audioSources.Add(newSource);
            return newSource;
        }

        protected AudioSource PlaySound(AudioClip sound)
        {
            AudioSource audioSource = GetAudioSource();
            audioSource.clip = sound;
            audioSource.Play();
            return audioSource;
        }

        public void Setup()
        {
            //_container = container;
            _musicSource = gameObject.AddComponent<AudioSource>();
            _musicSource.loop = true;
            _musicSource.playOnAwake = false;
            _audioSources = new List<AudioSource>();
        }

        public void Release()
        {
            //_container = null;
            _musicSource = null;
            _audioSources = null;
        }

        public void PlayMusic()
        {
            if (!_musicSource.isPlaying)
            {
                _musicSource.Play();
            }
        }

        public void PlayMusic(AudioClip clip)
        {
            _musicSource.clip = clip;
            _musicSource.Play();
        }

        public void PlayMusic(AudioClip clip, float fade)
        {
            _musicSource.clip = clip;
            _musicSource.Play();
            _musicSource.volume = 0;
            _musicSource.DOFade(1f, fade);
        }

        public void StopMusic()
        {
            _musicSource.Stop();
        }

        public void PlayTitleMusic()
        {
            PlayMusic(_titleMusicClip, 0.5f);
        }

        public void PlayGameMusic()
        {
            PlayMusic(_gameMusicClip);
        }

        public AudioSource PlayPopupOpenSound()
        {
            return PlaySound(_popupOpenClip);
        }

        public AudioSource PlayPopupCloseSound()
        {
            return PlaySound(_popupCloseClip);
        }

        public AudioSource PlaySelectSound()
        {
            return PlaySound(_selectClip);
        }

        public AudioSource PlayWarningSound()
        {
            return PlaySound(_warningClip);
        }

        public AudioSource PlayShowTextSound()
        {
            return PlaySound(_showTextClip);
        }

        public AudioSource PlayStartGameSound()
        {
            return PlaySound(_startGameClip);
        }

        public AudioSource PlayTakeDamageSound()
        {
            return PlaySound(_takeDamageClip);
        }

        public AudioSource PlayShotSound()
        {
            return PlaySound(_shotClip);
        }

        public AudioSource PlayPlayerDestroySound()
        {
            return PlaySound(_playerDestroyClip);
        }

        public AudioSource PlayEnemyTakeDamageSound()
        {
            return PlaySound(_enemyTakeDamageClip);
        }

        public AudioSource PlayEnemyDestroySound()
        {
            return PlaySound(_enemyDestroyClip);
        }

    }
}

