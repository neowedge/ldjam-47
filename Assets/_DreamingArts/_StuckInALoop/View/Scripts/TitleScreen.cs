﻿using DG.Tweening;
using Febucci.UI;
using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class TitleScreen : MonoBehaviour
    {
        [SerializeField]
        GameManager _gameManager;

        [SerializeField]
        CanvasGroup _screen;

        private CoroutineHandle _checkInputHandle;

        private void OnDestroy()
        {
            Timing.KillCoroutines(_checkInputHandle);
        }

        public void Show()
        {
            _screen.gameObject.SetActive(true);
            _checkInputHandle = Timing.CallContinuously(Mathf.Infinity, CheckInput);
        }

        void CheckInput()
        {
            if (Input.anyKeyDown)
            {
                ShowGame();
            }
        }

        private void ShowGame()
        {
            Timing.KillCoroutines(_checkInputHandle);

            _screen.gameObject.SetActive(false);
            _gameManager.ShowGame();
        }
    }
}