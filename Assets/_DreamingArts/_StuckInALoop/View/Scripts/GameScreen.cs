﻿using Febucci.UI;
using MEC;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class GameScreen : MonoBehaviour
    {
        public struct Command
        {
            string _name;
            System.Action _action;

            public string Name => _name;

            public Command(string name, System.Action action)
            {
                _name = name;
                _action = action;
            }

            public void Execute()
            {
                _action.Invoke();
            }
        }

        [SerializeField]
        GameManager _gameManager;

        [SerializeField]
        CanvasGroup _gameStatePanel;

        [SerializeField]
        TMP_Text _overclockText;

        [SerializeField]
        TMP_Text _endGameText;

        [SerializeField]
        TMP_Text _helpText;

        [SerializeField]
        TextVisibilityAnimation _textAnimator;

        List<Command> _currentCommands;

        CoroutineHandle _helpHandle;
        CoroutineHandle _checkInputHandle;

        public void Show()
        {
            _gameStatePanel.gameObject.SetActive(true);

            _textAnimator.AppendLine("Compiling...");
            _textAnimator.AppendLine();
            _textAnimator.AppendLine("<style=error>Error:</style> Process stuck in a loop.");

            _textAnimator.OnComplete += TextAnimator_OnComplete_StartPlayer;
            _textAnimator.Play();
        }

        [Button]
        public void ShowPlayerStatus()
        {
            string color = ColorUtility.ToHtmlStringRGB(_gameManager.Player.GetLifeIndicatorColor());

            _textAnimator.AppendLine();
            _textAnimator.AppendLine("--- GC status ---");
            _textAnimator.AppendLine("Cache: <style=value>" + _gameManager.Player.CurrentMemory.ToString("F1") + "</style> Mb.");
            _textAnimator.AppendLine("Cache used: <color=#" + color + ">" + _gameManager.Player.MemoryUsed.ToString("F2") + "</color> Mb. (<color=#" + color + ">" + _gameManager.Player.MemoryUsedPercent + "%</color>)");
            _textAnimator.AppendLine("Number of cores: <style=value>" + _gameManager.Player.CurrentWeapons + "</style>");
            _textAnimator.AppendLine("Core speed: <style=value>" + (_gameManager.Player.CurrentAngularSpeed / 360f).ToString("F1") + "</style> rnd/s.");
            _textAnimator.AppendLine("Slot size: <style=value>" + _gameManager.Player.CurrentDamage.ToString("F2") + "</style> Mb.");
            _textAnimator.AppendLine("Cleaning time: <style=value>" + _gameManager.Player.CurrentReloadDuration.ToString("F2") + "</style> s.");
        }

        [Button]
        public void ShowEndLevel()
        {
            _textAnimator.AppendLine();
            _textAnimator.AppendLine("Process recovered!");
            _textAnimator.AppendLine("<style=select>Garbage Collector</style> Berserk mode <style=error>DISABLED</style>");

            _textAnimator.AppendLine();
            _textAnimator.AppendLine("--- System overclocking ---");
            _currentCommands = new List<Command>();
            int number = 0;

            // MEMORY
            _currentCommands.Add(new Command("Cache", _gameManager.Player.UpgradeMemory));
            ++number;
            _textAnimator.AppendLine("<style=select>" + number + "</style>. Cache: <style=value>" + _gameManager.Player.CurrentMemory + "</style> Mb. => <style=upgrade>" + _gameManager.Player.CalculateUpgradeMemory() + "</style> Mb.");

            // ANGULAR SPEED
            if (_gameManager.Player.CanUpgradeWeapons())
            {
                _currentCommands.Add(new Command("Number of cores", _gameManager.Player.UpgradeWeapons));
                ++number;
                _textAnimator.AppendLine("<style=select>" + number + "</style>. Number of cores: <style=value>" + _gameManager.Player.CurrentWeapons + "</style> => <style=upgrade>" + _gameManager.Player.CalculateWeaponsUpgrade() + "</style>.");
            }

            if (_gameManager.Player.CanUpgradeAngularSpeed())
            {
                _currentCommands.Add(new Command("Core speed", _gameManager.Player.UpgradeAngularSpeed));
                ++number;
                _textAnimator.AppendLine("<style=select>" + number + "</style>. Core speed: <style=value>" + (_gameManager.Player.CurrentAngularSpeed / 360f).ToString("F1") + "</style> rnd/s. => <style=upgrade>" + (_gameManager.Player.CalculateAngularSpeedUpgrade() / 360f).ToString("F1") + "</style> rnd/s.");
            }

            if (_gameManager.Player.CanUpgradeDamage())
            {
                _currentCommands.Add(new Command("Slot size", _gameManager.Player.UpgradeDamage));
                ++number;
                _textAnimator.AppendLine("<style=select>" + number + "</style>. Slot size: <style=value>" + _gameManager.Player.CurrentDamage.ToString("F2") + "</style> Mb. => <style=upgrade>" + _gameManager.Player.CalculateDamageUpgrade().ToString("F2") + "</style> Mb.");
            }

            if (_gameManager.Player.CanUpgradeReloadDuration())
            {
                _currentCommands.Add(new Command("Cleaning time", _gameManager.Player.UpgradeReloadDuration));
                ++number;
                _textAnimator.AppendLine("<style=select>" + number + "</style>. Cleaning time: <style=value>" + _gameManager.Player.CurrentReloadDuration.ToString("F2") + "</style> s. => <style=upgrade>" + _gameManager.Player.CalculateReloadDurationUpgrade().ToString("F2") + "</style> s.");
            }
            _textAnimator.AppendLine();
            _textAnimator.AppendLine("Select a <style=select>number</style> to overclock.");

            _textAnimator.OnComplete += TextAnimator_OnComplete_Improves;
        }

        public void ShowEndGame()
        {
            _textAnimator.AppendLine();
            _textAnimator.AppendLine("<style=error>Stack overflow Exception</style>");
            _textAnimator.AppendLine("<style=select>Garbage Collector</style> Berserk mode <style=error>KILLED</style>");
            _textAnimator.AppendLine("|");

            _textAnimator.OnComplete += TextAnimator_OnComplete_EndGame;
        }

        public void ShowPlayerDamaged(string objectId, int damage)
        {
            string color = ColorUtility.ToHtmlStringRGB(_gameManager.Player.GetLifeIndicatorColor());

            _textAnimator.AppendLine();
            _textAnimator.AppendLine("<style=warning>Warning:</style> Memory allocated: <style=warning>" + damage.ToString("F2") + " Mb.</style> (instance id: " + objectId + ").");
            _textAnimator.AppendLine("Cache used: <color=#" + color + ">" + _gameManager.Player.MemoryUsed.ToString("F2") + "</color> Mb. / <style=value>" + _gameManager.Player.CurrentMemory.ToString("F2") + "</style> Mb. (<color=#" + color + ">" + _gameManager.Player.MemoryUsedPercent + "%</color>)");
        }

        private void ShowOvercLockText()
        {
            _overclockText.SetAlpha(1f);
            _overclockText.gameObject.SetActive(true);
            _helpHandle = Timing.CallDelayed(5f, ShowOverclockHelp, _helpText.gameObject);
        }

        private void ShowGameEndText()
        {
            _endGameText.gameObject.SetActive(true);
            _helpHandle = Timing.CallDelayed(5f, ShowGameOverHelp, _helpText.gameObject);
        }

        private void ShowOverclockHelp()
        {
            _helpText.text = "Press a number to select an overclock";
            _helpText.gameObject.SetActive(true);
        }

        private void ShowGameOverHelp()
        {
            _helpText.text = "Press any key to retry build";
            _helpText.gameObject.SetActive(true);
        }

        private void Upgrade(int commandIndex)
        {
            _gameManager.Audio.PlaySelectSound();

            Timing.KillCoroutines(_checkInputHandle);
            Timing.KillCoroutines(_helpHandle);

            _overclockText.gameObject.SetActive(false);
            _helpText.gameObject.SetActive(false);

            _currentCommands[commandIndex].Execute();

            _textAnimator.AppendLine();
            _textAnimator.AppendLine("<style=value>" + _currentCommands[commandIndex].Name + "</style> overclocked <style=upgrade>SUCCESSFULLY</style>!");

            _currentCommands = null;

            ShowPlayerStatus();

            _textAnimator.AppendLine();
            _textAnimator.AppendLine("Continue compiling...");
            _textAnimator.AppendLine();
            _textAnimator.AppendLine("<style=error>Error:</style> Process stuck in a loop.");
            _textAnimator.AppendLine("<style=select>Garbage Collector</style> Berserk mode <style=upgrade>ENABLED</style>");

            _textAnimator.OnComplete += TextAnimator_OnComplete_Continue;
        }

        private void TextAnimator_OnComplete_StartPlayer()
        {
            _textAnimator.OnComplete -= TextAnimator_OnComplete_StartPlayer;
            _gameManager.InitPlayer();
            _textAnimator.AppendLine("<style=select>Garbage Collector</style> Berserk mode <style=upgrade>ENABLED</style>");
            ShowPlayerStatus();
            _textAnimator.OnComplete += TextAnimator_OnComplete_StartGame;
        }

        private void TextAnimator_OnComplete_StartGame()
        {
            _textAnimator.OnComplete -= TextAnimator_OnComplete_StartGame;
            _gameManager.StartGame();
        }

        private void TextAnimator_OnComplete_Continue()
        {
            _textAnimator.OnComplete -= TextAnimator_OnComplete_Continue;
            _gameManager.NextLevel();
        }

        private void TextAnimator_OnComplete_Improves()
        {
            ShowOvercLockText();

            _textAnimator.OnComplete -= TextAnimator_OnComplete_Improves;
            _checkInputHandle = Timing.CallContinuously(Mathf.Infinity, CheckInput);
        }

        private void TextAnimator_OnComplete_EndGame()
        {
            ShowGameEndText();

            _textAnimator.OnComplete -= TextAnimator_OnComplete_EndGame;
            _checkInputHandle = Timing.CallContinuously(Mathf.Infinity, CheckInput);
        }

        private void CheckInput()
        {
            switch (_gameManager.CurrentState)
            {
                case GameManager.EState.Intro:
                    // TODO: saltar intro?
                    break;
                case GameManager.EState.Upgrade:
                    CheckUpgradeInput();
                    break;
                case GameManager.EState.EndGame:
                    CheckEndGameInput();
                    break;
            }
        }

        private void CheckUpgradeInput()
        {
            for (int i = 0; i < _currentCommands.Count; ++i)
            {
                int number = i + 1;
                KeyCode key;
                if (System.Enum.TryParse<KeyCode>("Alpha" + number, out key) && Input.GetKeyDown(key))
                {
                    Upgrade(i);
                    return;

                }
                else if (System.Enum.TryParse<KeyCode>("Keypad" + number, out key) && Input.GetKeyDown(key))
                {
                    Upgrade(i);
                    return;
                }
            }
        }

        private void CheckEndGameInput()
        {
            if (Input.anyKey)
            {
                Timing.KillCoroutines(_checkInputHandle);
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }
        }
    }
}