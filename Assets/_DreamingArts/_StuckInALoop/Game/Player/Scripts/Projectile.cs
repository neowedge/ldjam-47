﻿using MEC;
using Shapes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class Projectile : WorldActor
    {
        [SerializeField]
        Disc _renderer;

        [SerializeField]
        CircleCollider2D _collider;

        PlayerController _player;

        CoroutineHandle _coroutineHandle;

        public PlayerController Player => _player;

        private void OnDestroy()
        {
            Timing.KillCoroutines(_coroutineHandle);
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_coroutineHandle);
        }

        public void Init(PlayerController player)
        {
            _player = player;

            _renderer.Radius = 0.3f + (_player.CurrentDamage * 0.05f);
            _collider.radius = _renderer.Radius + 0.15f;

            gameObject.SetActive(true);
            _coroutineHandle = Timing.CallContinuously(Mathf.Infinity, UpgradePosition);
        }

        private void UpgradePosition()
        {
            T.position = T.position + T.up * _player.ProjectileSpeed * Time.deltaTime;

            if (T.position.y > 27f || T.position.y < -27f ||
                T.position.x > 27f || T.position.x < -27f)
            {
                Kill();
            }
        }

        public void Kill()
        {
            gameObject.SetActive(false);
        }
    }
}