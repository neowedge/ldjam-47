﻿using DG.Tweening;
using MEC;
using Shapes;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class PlayerController : WorldActor
    {
        private const float TAU = Mathf.PI * 2f;

        [SerializeField, BoxGroup("Settings")]
        private float _angularSpeed = 360f;

        [SerializeField, BoxGroup("Settings")]
        private float _reloadDuration = 0.5f;

        [SerializeField, BoxGroup("Settings")]
        private int _memory = 10;

        [SerializeField, BoxGroup("Settings")]
        private int _weapons = 1;

        [SerializeField, BoxGroup("Settings")]
        private int _damage = 1;

        [SerializeField, BoxGroup("Settings")]
        private float _projectileSpeed = 1f;

        [SerializeField, BoxGroup("Settings")]
        private Gradient _playerLifeIndicatorColor;

        [SerializeField, BoxGroup("Components")]
        private Transform _playerRotator;

        [SerializeField, BoxGroup("Components")]
        private Transform _weaponsContainer;

        [SerializeField, BoxGroup("Components")]
        private Transform[] _playerWeapons;

        [SerializeField, BoxGroup("Components")]
        private Disc _playerReloadIndicator;

        [SerializeField, BoxGroup("Components")]
        private Disc _playerLifeIndicator;

        [SerializeField, BoxGroup("Components")]
        private ParticleSystem _damageEffect;

        [SerializeField, BoxGroup("Components")]
        private Projectile _projectilePrefab;

        private GameManager _gameManager;

        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private List<Projectile> _projectilePool;

        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private int _currentWeapons;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private int _currentDamage;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private float _currentAngularSpeed;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private float _currentReloadDuration;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private int _currentMemory;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private int _memoryUsed;

        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private float _shotTime;
        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private float _reloadTime;

        private CoroutineHandle _checkInputHandle;
        private CoroutineHandle _checkReloadHandle;

        public int CurrentWeapons => _currentWeapons;
        public int CurrentDamage => _currentDamage;
        public float CurrentAngularSpeed => _currentAngularSpeed;
        public float CurrentReloadDuration => _currentReloadDuration;

        public int CurrentMemory => _currentMemory;
        public int MemoryUsed => _memoryUsed;
        public float MemoryUsedPercent => ((float)_memoryUsed / (float)_currentMemory) * 100f;

        public float ProjectileSpeed => _projectileSpeed;

        private void OnDestroy()
        {
            Timing.KillCoroutines(_checkInputHandle);
            Timing.KillCoroutines(_checkReloadHandle);
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_checkInputHandle);
            Timing.KillCoroutines(_checkReloadHandle);
        }

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Init()
        {
            _memoryUsed = 0;
            _currentMemory = _memory;
            _currentWeapons = _weapons;
            _currentDamage = _damage;
            _currentAngularSpeed = _angularSpeed;
            _currentReloadDuration = _reloadDuration;

            for (int i = 0; i < _playerWeapons.Length; ++i)
            {
                _playerWeapons[i].gameObject.SetActive(i < _currentWeapons);
            }

            _weaponsContainer.gameObject.SetActive(true);

            _projectilePool = new List<Projectile>();

            _checkInputHandle = Timing.CallContinuously(Mathf.Infinity, CheckInput);
            gameObject.SetActive(true);
        }

        public void Pause()
        {
            Timing.KillCoroutines(_checkInputHandle);
            _weaponsContainer.gameObject.SetActive(false);
        }

        public void Resume()
        {
            Timing.KillCoroutines(_checkInputHandle);
            _checkInputHandle = Timing.CallContinuously(Mathf.Infinity, CheckInput);
            _weaponsContainer.gameObject.SetActive(true);
        }

        private void CheckInput()
        {
            float currentAngularSpeed = 0;
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                currentAngularSpeed += CurrentAngularSpeed;
            }
            if (Input.GetKey(KeyCode.D) | Input.GetKey(KeyCode.RightArrow))
            {
                currentAngularSpeed -= CurrentAngularSpeed;
            }
            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Return))
            {
                TryShot();
            }

            if (currentAngularSpeed != 0)
            {
                _playerRotator.Rotate(0, 0, currentAngularSpeed * Time.deltaTime);
            }
        }

        private void CheckReload()
        {
            if (_playerReloadIndicator == null || _playerReloadIndicator.gameObject == null)
            {
                Timing.KillCoroutines(_checkReloadHandle);
                Timing.KillCoroutines(_checkInputHandle);
                return;
            }

            if (Time.time > _reloadTime)
            {
                _playerReloadIndicator.AngRadiansEnd = TAU;
                Timing.KillCoroutines(_checkReloadHandle);
            }
            else
            {
                _playerReloadIndicator.AngRadiansEnd = TAU * (Time.time - _shotTime) / (_reloadTime - _shotTime);
            }
        }

        private void TryShot()
        {
            if (Time.time >= _reloadTime)
            {
                Shot();
            }
        }

        private void Shot()
        {
            _gameManager.Audio.PlayShotSound();

            for (int i = 0; i < CurrentWeapons; ++i)
            {
                DOTween.Sequence()
                        .Append(_playerWeapons[i].DOLocalMoveY(1.2f, 0.02f).SetEase(Ease.Flash))
                        .Append(_playerWeapons[i].DOLocalMoveY(2f, 0.08f));

                InstantiateProjectile(_playerWeapons[i]);
            }
            _shotTime = Time.time;
            _reloadTime = _shotTime + CurrentReloadDuration;

            _checkReloadHandle = Timing.CallContinuously(Mathf.Infinity, CheckReload);

            _playerReloadIndicator.AngRadiansEnd = 0;
        }

        private Projectile InstantiateProjectile(Transform playerWeapon)
        {
            for (int i = 0; i < _projectilePool.Count; ++i)
            {
                if (!_projectilePool[i].gameObject.activeSelf)
                {
                    _projectilePool[i].T.position = playerWeapon.position + playerWeapon.up;
                    _projectilePool[i].T.rotation = playerWeapon.rotation;
                    _projectilePool[i].Init(this);
                    return _projectilePool[i];
                }
            }
            Projectile projectile = Instantiate(_projectilePrefab, playerWeapon.position + playerWeapon.up, playerWeapon.rotation);
            _projectilePool.Add(projectile);

            projectile.Init(this);
            return projectile;
        }

        public Color GetLifeIndicatorColor()
        {
            return _playerLifeIndicatorColor.Evaluate((float)_memoryUsed / (float)_currentMemory);
        }

        [Button]
        public void TakeDamage(string id, int damage)
        {
            if (_memoryUsed < _currentMemory)
            {
                _memoryUsed += damage;

                if (_memoryUsed < _currentMemory)
                {
                    _playerLifeIndicator.Color = GetLifeIndicatorColor();

                    _gameManager.PlayerDamaged(id, damage);

                    _gameManager.Audio.PlayTakeDamageSound();
                    _gameManager.Camera.DOShakePosition(0.1f, 0.3f, 50, 90, false);
                }
                else
                {
                    _playerLifeIndicator.Color = _playerLifeIndicatorColor.Evaluate(1f);

                    _gameManager.Audio.PlayPlayerDestroySound();
                    _gameManager.Camera.DOShakePosition(0.5f, 0.5f + damage * 0.15f, 80 + damage * 5, 90, false);

                    Timing.KillCoroutines(_checkInputHandle);
                    _weaponsContainer.gameObject.SetActive(false);
                    _gameManager.EndGame();
                }
                _damageEffect.Play();
            }
        }

        public int CalculateUpgradeMemory()
        {
            return _currentMemory + 10;
        }

        public int CalculateDamageUpgrade()
        {
            return CurrentDamage + 1;
        }

        public int CalculateWeaponsUpgrade()
        {
            return CurrentWeapons + 1;
        }

        public float CalculateReloadDurationUpgrade()
        {
            return CurrentReloadDuration * 0.8f;
        }

        public float CalculateAngularSpeedUpgrade()
        {
            return CurrentAngularSpeed + 45f;
        }

        public bool CanUpgradeWeapons()
        {
            return _currentWeapons < _playerWeapons.Length;
        }

        public bool CanUpgradeReloadDuration()
        {
            return _reloadDuration > 0.2f;
        }

        public bool CanUpgradeAngularSpeed()
        {
            return _angularSpeed < 720f; // 360 * 2
        }

        public bool CanUpgradeDamage()
        {
            return _damage < 10;
        }

        [Button]
        public void UpgradeWeapons()
        {
            _currentWeapons = CalculateWeaponsUpgrade();
            for (int i = 0; i < _playerWeapons.Length; ++i)
            {
                _playerWeapons[i].gameObject.SetActive(i < _currentWeapons);
            }
        }

        [Button]
        public void UpgradeMemory()
        {
            _currentMemory = CalculateUpgradeMemory();
            _playerLifeIndicator.Color = _playerLifeIndicatorColor.Evaluate((float)_memoryUsed / (float)_currentMemory);
        }

        [Button]
        public void UpgradeDamage()
        {
            _currentDamage = CalculateDamageUpgrade();
        }

        [Button]
        public void UpgradeReloadDuration()
        {
            _currentReloadDuration = CalculateReloadDurationUpgrade();
        }

        [Button]
        public void UpgradeAngularSpeed()
        {
            _currentAngularSpeed = CalculateAngularSpeedUpgrade();
        }
    }
}