﻿using MEC;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class GameManager : MonoBehaviour
    {
        public enum EState
        {
            TitleScreen,
            Intro,
            Playing,
            Upgrade,
            EndGame
        }

        [SerializeField, BoxGroup("Controllers")]
        private AudioManager _audio;

        [SerializeField, BoxGroup("Controllers")]
        private PlayerController _player;

        [SerializeField, BoxGroup("Controllers")]
        private EnemiesSpawner _enemies;

        [SerializeField, BoxGroup("View")]
        private Camera _camera;

        [SerializeField, BoxGroup("View")]
        private TitleScreen _titleScreen;

        [SerializeField, BoxGroup("View")]
        private GameScreen _gameScreen;

        private int _currentLevel = 1;

        EState _currentState;

        public AudioManager Audio => _audio;
        public PlayerController Player => _player;

        public Camera Camera => _camera;
        public int CurrentLevel => _currentLevel;
        public EState CurrentState => _currentState;

        public System.Action OnStart;

        private void Start()
        {
            Cursor.visible = false;
            _camera = Camera.main;

            _audio.Setup();
            _player.Setup(this);
            _enemies.Setup(this);

            StartTitle();
        }

        private void StartTitle()
        {
            _currentState = EState.TitleScreen;

            _audio.PlayTitleMusic();
            _titleScreen.Show();
        }

        public void ShowGame()
        {
            _audio.PlayGameMusic();

            _currentState = EState.Intro;

            _gameScreen.Show();
        }

        public void EndLevel()
        {
            if (_currentState == EState.Playing)
            {
                _gameScreen.ShowEndLevel();
                _currentState = EState.Upgrade;

                Timing.CallDelayed(1f, _player.Pause);
            }
        }

        public void NextLevel()
        {
            _currentState = EState.Playing;

            ++_currentLevel;
            _enemies.Init();
            _player.Resume();
        }

        public void InitPlayer()
        {
            _player.Init();
        }

        public void StartGame()
        {
            _currentState = EState.Playing;
            _enemies.Init();
        }

        public void EndGame()
        {
            if (_currentState == EState.Playing)
            {
                _currentState = EState.EndGame;
                _gameScreen.ShowEndGame();
            }
        }

        public int GetLevelEnemies()
        {
            return 10 + CurrentLevel * 10;
        }

        public int GetEnemyMaxLifes()
        {
            return Mathf.Min(Mathf.FloorToInt((CurrentLevel - 1) / 3f) + 1, 16);
        }

        public float GetLevelTimeBetweenEnemies()
        {
            return 1f * Mathf.Pow(0.9f, (float)(CurrentLevel - 1));
        }

        internal void PlayerDamaged(string id, int damage)
        {
            _gameScreen.ShowPlayerDamaged(id, damage);
        }
    }
}