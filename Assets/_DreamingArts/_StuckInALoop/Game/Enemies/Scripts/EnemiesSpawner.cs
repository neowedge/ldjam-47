﻿using MEC;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class EnemiesSpawner : MonoBehaviour
    {
        [SerializeField]
        private Enemy _enemyPrefab;

        private GameManager _gameManager;

        [ShowInInspector, HideInEditorMode, BoxGroup("State")]
        private List<Enemy> _enemiesPool;

        public GameManager GameManager => _gameManager;

        private int _enemiesLeft;
        private int _enemiesAlive;
        private float _timeBetweenEnemies;

        public void Setup(GameManager gameManager)
        {
            _gameManager = gameManager;
            _enemiesPool = new List<Enemy>();
        }

        [Button]
        public void Spawn()
        {
            int maxLifes = _gameManager.GetEnemyMaxLifes();
            int lifes = Random.Range(Mathf.Max(1, maxLifes - 3), maxLifes + 1);

            Vector3 position = GetSpawnPosition(lifes);

            var rotForwardToDown = Quaternion.FromToRotation(Vector3.forward, Vector3.up);

            //Vector3 dir = _gameManager.Player.T.position - position;
            //dir.z = 0;
            //float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90f;
            //Quaternion rotation = Quaternion.LookRotation(Vector3.forward, dir);

            Enemy enemy = null;
            for (int i = 0; i < _enemiesPool.Count; ++i)
            {
                if (!_enemiesPool[i].gameObject.activeSelf)
                {
                    enemy = _enemiesPool[i];
                    enemy.T.position = position;
                    //_enemiesPool[i].T.rotation = rotation;
                    break;
                }
            }

            if (enemy == null)
            {
                enemy = Instantiate(_enemyPrefab, GetSpawnPosition(lifes), Quaternion.identity);
                _enemiesPool.Add(enemy);
            }

            enemy.Init(this, lifes);

            --_enemiesLeft;
            if (_enemiesLeft > 0)
            {
                Timing.CallDelayed(_timeBetweenEnemies, Spawn, gameObject);
            }
        }

        public Vector3 GetSpawnPosition(int lifes)
        {
            int zone = Random.Range(0, 4);

            float margin = 24f + lifes;

            switch (zone)
            {
                case 0:
                    // TOP
                    return new Vector3(Random.Range(-margin, margin), margin, 0);

                case 1:
                    // BOTTOM
                    return new Vector3(Random.Range(-margin, margin), -margin, 0);

                case 2:
                    // LEFT
                    return new Vector3(-margin, Random.Range(-margin, margin), 0);

                case 3:
                    // RIGHT
                    return new Vector3(margin, Random.Range(-margin, margin), 0);
            }

            return Vector2.zero;
        }

        public void EnemyKilled()
        {
            --_enemiesAlive;
            if (_enemiesAlive == 0)
            {
                _gameManager.EndLevel();
            }
        }

        public void Init()
        {
            _enemiesLeft = _gameManager.GetLevelEnemies();
            _enemiesAlive = _enemiesLeft;
            _timeBetweenEnemies = _gameManager.GetLevelTimeBetweenEnemies();

            Timing.CallDelayed(1f, Spawn, gameObject);
        }
    }
}