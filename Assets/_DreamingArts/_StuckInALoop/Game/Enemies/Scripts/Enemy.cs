﻿using DG.Tweening;
using MEC;
using Shapes;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.DreamingArts.StuckInALoop
{
    public class Enemy : WorldActor
    {
        [SerializeField]
        Transform _rendererT;

        [SerializeField]
        RegularPolygon _renderer;

        [SerializeField]
        CircleCollider2D _collider;

        [SerializeField, BoxGroup("Components")]
        private ParticleSystem _damageEffect;

        EnemiesSpawner _spawner;

        Tween _rotateTween;
        Tween _moveTween;
        //CoroutineHandle _coroutineHandle;

        private bool _isKilled;

        private int _startLifes;
        private int _currentLifes;

        public int StartLifes => _startLifes;
        public int CurrentLifes => _currentLifes;

        public string Id => GetInstanceID().ToString();

        private void OnDisable()
        {
            Stop();
            //Timing.KillCoroutines(_coroutineHandle);
        }

        private void OnDestroy()
        {
            Stop();

            //Timing.KillCoroutines(_coroutineHandle);
        }

        [Button]
        public void Init(EnemiesSpawner spawner, int lifes)
        {
            _spawner = spawner;
            _startLifes = lifes;
            _currentLifes = _startLifes;

            SetSize();

            _isKilled = false;
            _collider.enabled = _collider.enabled = true;
            T.localScale = Vector3.one;
            _rotateTween = _rendererT.DOLocalRotate(new Vector3(0, 0, -360f), _currentLifes, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);

            _rendererT.gameObject.SetActive(true);
            gameObject.SetActive(true);
            //_coroutineHandle = Timing.CallContinuously(Mathf.Infinity, UpgradePosition);

            float duration = Vector3.Distance(T.position, _spawner.GameManager.Player.T.position) / (6f - _currentLifes * 0.25f);
            _moveTween = T.DOMove(_spawner.GameManager.Player.T.position, duration).SetEase(Ease.Linear);
        }

        //private void CheckCollisions()
        //{
        //    if (T.position.magnitude < 1f)
        //    {
        //        _spawner.GameManager.Player.TakeDamage(_currentLifes);
        //        gameObject.SetActive(false);
        //    }
        //}

        //private void UpgradePosition()
        //{
        //    T.position = T.position + T.up * (10f - _currentLifes * 0.5f) * Time.deltaTime;

        //    if (T.position.magnitude < 1f)
        //    {
        //        _spawner.GameManager.Player.TakeDamage(_currentLifes);
        //        gameObject.SetActive(false);
        //    }
        //}

        [Button]
        public void TakeDamage(int damage)
        {
            _currentLifes -= damage;

            _damageEffect.Play();

            if (_currentLifes > 0)
            {
                _spawner.GameManager.Audio.PlayEnemyTakeDamageSound();

                SetSize();

                if (_rotateTween != null)
                {
                    _rotateTween.Kill();
                }
                _rotateTween = _rendererT.DOLocalRotate(new Vector3(0, 0, -360f), _currentLifes, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);

                _spawner.GameManager.Camera.DOShakePosition(0.05f, 0.2f, 30, 90, false);
            }
            else
            {
                _spawner.GameManager.Audio.PlayEnemyDestroySound();

                //Timing.KillCoroutines(_coroutineHandle);
                Stop();
                _rendererT.gameObject.SetActive(false);

                _spawner.GameManager.Camera.DOShakePosition(0.06f, 0.3f + _currentLifes * 0.1f , 50 + _currentLifes * 5, 90, false);

                _spawner.EnemyKilled();
                _isKilled = true;

                Timing.CallDelayed(0.5f, () =>
                {
                    gameObject.SetActive(false);
                });
            }
        }

        private void SetSize()
        {
            _renderer.Sides = _currentLifes + 2;
            _renderer.Radius = 1f + _renderer.Sides / 6f;
            _collider.radius = _renderer.Radius + 0.15f;
        }

        private void Stop()
        {
            if (_rotateTween != null)
            {
                _rotateTween.Kill();
                _rotateTween = null;
            }
            if (_moveTween != null)
            {
                _moveTween.Kill();
                _moveTween = null;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!_isKilled && collision.gameObject.activeInHierarchy) //A projectile could send 2 triggers in the same frame, so the second one is not active
            {
                Projectile projectile = collision.GetComponentInParent<Projectile>();
                if (projectile != null)
                {
                    TakeDamage(projectile.Player.CurrentDamage);
                    projectile.Kill();
                }
                else
                {
                    PlayerController player = collision.GetComponentInParent<PlayerController>();
                    if (player == null)
                    {
                        // Security check, but with active comprobation will never pass here
                        Debug.LogWarning("Collider is not a projectile nor player!");
                        return;
                    }

                    if (_moveTween != null)
                    {
                        _moveTween.Kill();
                        _moveTween = null;
                    }

                    _collider.enabled = false;

                    _moveTween = DOTween.Sequence()
                        .Append(T.DOMove(player.T.position, 0.1f)).SetEase(Ease.Flash)
                        .Join(T.DOScale(0, 0.1f).SetEase(Ease.InQuad))
                        .OnComplete(() =>
                        {
                            gameObject.SetActive(false);
                            _spawner.EnemyKilled();
                            player.TakeDamage(Id, _currentLifes);
                        });

                    _isKilled = true;
                }
            }
        }
    }
}