﻿#region copyright
//--------------------------------------------------------------------
// Copyright (C) 2015 Dmitriy Yukhanov - focus [http://codestage.net].
//--------------------------------------------------------------------
#endregion

namespace CodeStage.Maintainer.UI
{
	using Cleaner;
	using Issues;
	using Tools;
	using References;
	using UnityEditor;
	using UnityEngine;

	public class MaintainerMenu
	{
		public const string ReferencesFinderMenuItemName = "🔎 Find References in Project";
		public const string ScriptReferencesFinderMenuItemName = "CONTEXT/MonoBehaviour/🔎 Maintainer: Find All Script References"; 
		public const string ProjectBrowserContext = "Assets/⚙ Maintainer/" + ReferencesFinderMenuItemName + " %#&s";
		public const string MainMenu = "Tools/Code Stage/⚙ Maintainer/";

		[MenuItem(MainMenu + "Show %#&`", false, 900)]
		private static void ShowWindow()
		{
			MaintainerWindow.Create();
		}

		[MenuItem(MainMenu + "About", false, 901)]
		private static void ShowAbout()
		{
			MaintainerWindow.ShowAbout();
		}

		[MenuItem(MainMenu + "Find Issues %#&f", false, 1000)]
		private static void FindAllIssues()
		{
			IssuesFinder.StartSearch(true);
		}

		[MenuItem(MainMenu + "Find Garbage %#&g", false, 1001)]
		private static void FindAllGarbage()
		{
			ProjectCleaner.StartSearch(true);
		}

#if UNITY_5_6_OR_NEWER
		[MenuItem(MainMenu + "Find All References %#&r", false, 1002)]
		private static void FindAllReferences()
		{
			ReferencesFinder.GetReferences();
		}

		[MenuItem(ProjectBrowserContext, true, 39)]
		public static bool ValidateFindReferences()
		{
			return CSEditorTools.GetProjectSelections(true).Length > 0;
		}

		[MenuItem(ProjectBrowserContext, false, 39)]
		public static void FindReferences()
		{
			ReferencesFinder.AddSelectedToSelectionAndRun();
		}

		[MenuItem(ScriptReferencesFinderMenuItemName, true, 144444)]
		public static bool ValidateFindComponentReferences(MenuCommand command)
		{
			var scriptPath = CSObjectTools.GetScriptPathFromMonoBehaviour(command.context as MonoBehaviour);
			return !string.IsNullOrEmpty(scriptPath);
		}

		[MenuItem(ScriptReferencesFinderMenuItemName, false, 144444)]
		public static void FindComponentReferences(MenuCommand command)
		{
			var scriptPath = CSObjectTools.GetScriptPathFromMonoBehaviour(command.context as MonoBehaviour);
			ReferencesFinder.AddToSelectionAndRun(scriptPath);
		}
#endif
	}
}